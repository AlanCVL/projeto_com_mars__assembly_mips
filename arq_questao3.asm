.data
 VALOR1: .word 5
 VALOR2: .word 10
 texto1: .asciiz "numero pego em VALOR1: "
 texto2: .asciiz "numero pego em VALOR2: "
 pulalinha: .asciiz "\n"
 
.text

lw $a0, VALOR1 #carrega valor de VALOR1
lw $a1, VALOR2 #carrega valor de VALOR2
jal swap #chama a funcao swap

li $v0, 4 #print num int
la $a0, texto1 #carrega texto
syscall

li $v0, 1 #print numint
lw $a0, VALOR1 #carrega valor dde VALOR1
syscall

li $v0, 4 #print num int
la $a0, pulalinha #carrega pulaLinha
syscall

la $a0, texto2 #carrega texto
syscall

li $v0, 1 #print numint
lw $a0, VALOR2 #carrega valor dde VALOR1
syscall

li $v0, 10 #encerra programa (sem ele da loop inifinto, nao remover linha)
syscall

swap:
  addiu $sp, $sp, -4
  sw $ra, 0($sp)
  
  sw $a0, VALOR2 #escreve em VALOR2 o valor do reg $a0
  sw $a1, VALOR1 #escreve em VALOR1 o valor do reg $a1
  
  lw $ra, 0($sp)
  addiu $sp, $sp, 4
  jr $ra
