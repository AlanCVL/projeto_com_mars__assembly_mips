.data
asteristico: .asciiz "*"
pulaLinha: .asciiz "\n"
input: .asciiz "informe o tamanho:"

.text

li $v0, 4 #print texto
la $a0, input #carrega texto input
syscall

li $v0, 5 #scanf do mips que valor sera guardado no $v0
syscall
move $s0, $v0 #copia valor do input para $s0

li $t0, 0 #t0 = 0

loop:
    beq $t0, $s0, end #if t0 == tamanho -> end
    li $t1, 0 #t1=0
    move $t2, $s0 #t2 = tam
    sub $t2, $t2, $t0 #t2 = t2 - t0
loopInside:
    beq $t1, $t2, endInside #if t1==t2 ->endInside
    li $v0, 4 #print text
    la $a0, asteristico #carrega asteristico
    syscall
    addi $t1, $t1, 1 #t1++
    j loopInside

endInside:
    la $a0, pulaLinha #carrega texto pulaLinha
    syscall
    addi $t0, $t0, +1 #t0++
    j loop

end:



#int main() {
#   char asteristico = "*";
#   int tamanho =5;
#   for(int i=0; i< tamanho; i++){
#       for(int j=0; j < tamanho -i ; j++){
#           printf("%c", asteristic);
#       }
#       printf("\n");
#   }
#}

