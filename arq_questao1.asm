.data
asteristico: .asciiz "*"
pulaLinha: .asciiz "\n"

.text

li $s0, 5 #tamanho = 5
li $t0, 1 #t0 = 1

loop:
    bgt $t0, $s0, end #if t0 > tamanho -> end
    li $t1, 1 #t1=0
loopInside:
    bgt $t1, $t0, endInside #if t1>t0 ->endInside
    li $v0, 4 #print text
    la $a0, asteristico #carrega asteristico
    syscall
    addi $t1, $t1, 1 #t1++
    j loopInside

endInside:
    la $a0, pulaLinha #carrega texto pulaLinha
    syscall
    addi $t0, $t0, +1 #t0++
    j loop

end:



#int main() {
#   char asteristico = "*";
#   int tamanho =5;
#   for(int i=1; i<= tamanho; i++){
#       for(int j=1; j>i; j++){
#           printf("%c", asteristic);
#       }
#       printf("\n");
#   }
#}

