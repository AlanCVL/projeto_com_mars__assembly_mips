.data
asteristico: .asciiz "*"
pulaLinha: .asciiz "\n"
input1: .asciiz "digite o num 1: "
input2: .asciiz "digite o num 2: "
input3: .asciiz "digite o num 3: "
input4: .asciiz "digite o num 4: "
resultado: .asciiz "Resultado da Soma: "
.text

#----------input de  num 1
li $v0, 4 #print texto
la $a0, input1 #carrega texto input1
syscall

li $v0, 5 #scanf do mips que valor sera guardado no $v0
syscall
move $t0, $v0 #copia valor do input para $t0

#----------input de  num 2
li $v0, 4 #print texto
la $a0, input2 #carrega texto input2
syscall
li $v0, 5 #scanf do mips que valor sera guardado no $v0
syscall
move $t1, $v0 #copia valor do input para $t1

#----------input de  num 3
li $v0, 4 #print texto
la $a0, input3 #carrega texto input3
syscall

li $v0, 5 #scanf do mips que valor sera guardado no $v0
syscall
move $t2, $v0 #copia valor do input para $t2

#----------input de  num 4
li $v0, 4 #print texto
la $a0, input4 #carrega texto input4
syscall

li $v0, 5 #scanf do mips que valor sera guardado no $v0
syscall
move $t3, $v0 #copia valor do input para $t3

#--------Fim dos Inputs

#copio os valores e coloco em $a0 ... $a3
move $a0, $t0 #a0 = t0
move $a1, $t1#a1 = t1
move $a2 $t2#a2 = t2
move $a3, $t3#a3 = t3

jal SOMA4 #chama a funcao SOMA4
move $a1, $v0 #a1 = result de SOMA4

li $v0, 4 #print texto
la $a0, resultado #carrega texto de resultado
syscall

move $a0, $a1 #a0 = a1
li $v0, 1#print de int
syscall

li $v0, 10 #encerra programa
syscall

SOMA4:
	move $t2, $ra #t2 = ra
	jal CALCSOMA #chama CalcSoma
	move $t0, $s0 #resultado de CALCSOMA4 copiado para t0
	
	move $a0, $a2 #a0 = a2
	move $a1, $a3 #a1 = a3
	jal CALCSOMA #chama  CalcSoma
	move $t1, $s0 #resultado de CALCSOMA4 copiado para t1
	add $v0, $t0, $t1 #v0 = t0+t1
	move $ra, $t2 #ra = t2
	jr $ra
	
CALCSOMA:
	add $s0, $a0, $a1 
	jr $ra
